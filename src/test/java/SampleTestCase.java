import com.google.gson.Gson;
import helpers.MoonPhases;
import kong.unirest.Unirest;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import shema.ForecastResult;


public class SampleTestCase {

    @Test
    public void UnirestTest() {

        String response = Unirest.get("https://api.weather.yandex.ru/v1/forecast?lat=55.755833&lon=37.617778&lang=ru_RU&limit=2&hours=false&extra=false")
                .header("X-Yandex-API-Key", "d44a8297-9089-4220-9ee7-3db740090068")
                .asString().getBody();

        Gson g = new Gson();
        ForecastResult jsonResult = g.fromJson(response, ForecastResult.class);
        MoonPhases moonText = new MoonPhases();

        Assertions.assertThat(jsonResult.getInfo().lat).isEqualTo(55.755833f);
        Assertions.assertThat(jsonResult.getInfo().lon).isEqualTo(37.617778f);
        Assertions.assertThat(jsonResult.getInfo().tzinfo.offset).isEqualTo(10800);
        Assertions.assertThat(jsonResult.getInfo().tzinfo.name).isEqualTo("Europe/Moscow");
        Assertions.assertThat(jsonResult.getInfo().tzinfo.dst).isEqualTo(false);
        Assertions.assertThat(jsonResult.getInfo().url).isEqualTo("https://yandex.ru/pogoda/?lat=55.755833&lon=37.617778");
        Assertions.assertThat(jsonResult.getForecasts()).hasSize(2);
        Assertions.assertThat(jsonResult.getFact().season).isEqualTo("autumn");
        Assertions.assertThat(moonText.getMoonTextByMoonCode(jsonResult.getForecasts().get(1).moonCode)).isEqualTo("decreasing-moon");
    }
}
