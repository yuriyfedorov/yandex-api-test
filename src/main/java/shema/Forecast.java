package shema;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "date",
        "date_ts",
        "week",
        "sunrise",
        "sunset",
        "rise_begin",
        "set_end",
        "moon_code",
        "moon_text",
        "parts"
})
public class Forecast {

    @JsonProperty("date")
    public String date;
    @JsonProperty("date_ts")
    public int dateTs;
    @JsonProperty("week")
    public int week;
    @JsonProperty("sunrise")
    public String sunrise;
    @JsonProperty("sunset")
    public String sunset;
    @JsonProperty("rise_begin")
    public String riseBegin;
    @JsonProperty("set_end")
    public String setEnd;
    @JsonProperty("moon_code")
    public int moonCode;
    @JsonProperty("moon_text")
    public String moonText;
    @JsonProperty("parts")
    public Parts parts;

    public String getDate() { return date; }
    public int getDateTs() { return dateTs; }
    public int getWeek() { return week; }
    public String getSunrise() { return sunrise; }
    public String getSunset() { return sunset; }
    public String getRiseBegin() { return riseBegin; }
    public String getSetEnd() { return setEnd; }
    public int getMoonCode() { return moonCode; }
    public String getMoonText() { return moonText; }
    public Parts getParts() { return parts; }
}