package shema;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
		"temp",
		"feels_like",
		"icon",
		"condition",
		"wind_speed",
		"wind_gust",
		"wind_dir",
		"pressure_mm",
		"pressure_pa",
		"humidity",
		"uv_index",
		"soil_temp",
		"soil_moisture",
		"daytime",
		"polar",
		"season",
		"obs_time",
		"source"
})
public class Fact {

	@JsonProperty("temp")
	public int temp;
	@JsonProperty("feels_like")
	public int feelsLike;
	@JsonProperty("icon")
	public String icon;
	@JsonProperty("condition")
	public String condition;
	@JsonProperty("wind_speed")
	public int windSpeed;
	@JsonProperty("wind_gust")
	public int windGust;
	@JsonProperty("wind_dir")
	public String windDir;
	@JsonProperty("pressure_mm")
	public int pressureMm;
	@JsonProperty("pressure_pa")
	public int pressurePa;
	@JsonProperty("humidity")
	public int humidity;
	@JsonProperty("uv_index")
	public int uvIndex;
	@JsonProperty("soil_temp")
	public int soilTemp;
	@JsonProperty("soil_moisture")
	public int soilMoisture;
	@JsonProperty("daytime")
	public String daytime;
	@JsonProperty("polar")
	public boolean polar;
	@JsonProperty("season")
	public String season;
	@JsonProperty("obs_time")
	public int obsTime;
	@JsonProperty("source")
	public String source;

	public boolean isPolar(){
		return polar;
	}
	public int getTemp(){
		return temp;
	}
	public String getIcon(){
		return icon;
	}
	public int getPressureMm(){
		return pressureMm;
	}
	public String getWindDir(){
		return windDir;
	}
	public String getSource(){
		return source;
	}
	public int getFeelsLike(){
		return feelsLike;
	}
	public int getWindGust(){
		return windGust;
	}
	public String getCondition(){
		return condition;
	}
	public int getUvIndex(){
		return uvIndex;
	}
	public int getPressurePa(){
		return pressurePa;
	}
	public int getHumidity(){
		return humidity;
	}
	public String getSeason(){
		return season;
	}
	public int getWindSpeed(){
		return windSpeed;
	}
	public double getSoilMoisture(){
		return soilMoisture;
	}
	public String getDaytime(){
		return daytime;
	}
	public int getSoilTemp(){
		return soilTemp;
	}
	public int getObsTime(){
		return obsTime;
	}
}