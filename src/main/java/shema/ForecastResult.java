package shema;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "now",
        "now_dt",
        "info",
        "fact",
        "forecasts"
})
public class ForecastResult{

    @JsonProperty("now_dt")
    private String nowDt;
    @JsonProperty("fact")
    private Fact fact;
    @JsonProperty("now")
    private int now;
    @JsonProperty("info")
    private Info info;
    @JsonProperty("forecasts")
    private ArrayList<Forecast> forecasts;

    public String getNowDt(){
        return nowDt;
    }
    public Fact getFact(){
        return fact;
    }
    public int getNow(){
        return now;
    }
    public Info getInfo(){
        return info;
    }
    public ArrayList<Forecast> getForecasts(){
        return forecasts;
    }
}