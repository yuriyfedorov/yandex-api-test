package shema;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "_source",
        "temp_min",
        "temp_max",
        "temp_avg",
        "feels_like",
        "icon",
        "condition",
        "daytime",
        "polar",
        "wind_speed",
        "wind_gust",
        "wind_dir",
        "pressure_mm",
        "pressure_pa",
        "humidity",
        "uv_index",
        "soil_temp",
        "soil_moisture",
        "prec_mm",
        "prec_period",
        "prec_prob"
})

public class PeriodOfTime {

    @JsonProperty("polar")
    private boolean polar;
    @JsonProperty("icon")
    private String icon;
    @JsonProperty("pressure_mm")
    private int pressureMm;
    @JsonProperty("prec_period")
    private int precPeriod;
    @JsonProperty("wind_dir")
    private String windDir;
    @JsonProperty("temp_max")
    private int tempMax;
    @JsonProperty("feels_like")
    private int feelsLike;
    @JsonProperty("wind_gust")
    private double windGust;
    @JsonProperty("temp_min")
    private int tempMin;
    @JsonProperty("condition")
    private String condition;
    @JsonProperty("temp_avg")
    private int tempAvg;
    @JsonProperty("uv_index")
    private int uvIndex;
    @JsonProperty("pressure_pa")
    private int pressurePa;
    @JsonProperty("humidity")
    private int humidity;
    @JsonProperty("_source")
    private String source;
    @JsonProperty("wind_speed")
    private double windSpeed;
    @JsonProperty("soil_moisture")
    private double soilMoisture;
    @JsonProperty("daytime")
    private String daytime;
    @JsonProperty("soil_temp")
    private int soilTemp;
    @JsonProperty("prec_mm")
    private double precMm;
    @JsonProperty("prec_prob")
    private int precProb;

    public boolean isPolar() {
        return polar;
    }
    public String getIcon() {
        return icon;
    }
    public int getPressureMm() {
        return pressureMm;
    }
    public int getPrecPeriod() {
        return precPeriod;
    }
    public String getWindDir() {
        return windDir;
    }
    public int getTempMax() {
        return tempMax;
    }
    public int getFeelsLike() {
        return feelsLike;
    }
    public double getWindGust() {
        return windGust;
    }
    public int getTempMin() {
        return tempMin;
    }
    public String getCondition() {
        return condition;
    }
    public int getTempAvg() {
        return tempAvg;
    }
    public int getUvIndex() {
        return uvIndex;
    }
    public int getPressurePa() {
        return pressurePa;
    }
    public int getHumidity() {
        return humidity;
    }
    public String getSource() {
        return source;
    }
    public double getWindSpeed() {
        return windSpeed;
    }
    public double getSoilMoisture() {
        return soilMoisture;
    }
    public String getDaytime() {
        return daytime;
    }
    public int getSoilTemp() {
        return soilTemp;
    }
    public double getPrecMm() {
        return precMm;
    }
    public int getPrecProb() {
        return precProb;
    }
}