package shema;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
		"night",
		"morning",
		"day",
		"evening",
		"day_short",
		"night_short"
})
public class Parts{

	@JsonProperty("night")
	private PeriodOfTime night;
	@JsonProperty("day_short")
	private PeriodOfTime dayShort;
	@JsonProperty("evening")
	private PeriodOfTime evening;
	@JsonProperty("day")
	private PeriodOfTime day;
	@JsonProperty("night_short")
	private PeriodOfTime nightShort;
	@JsonProperty("morning")
	private PeriodOfTime morning;

	public PeriodOfTime getNight(){
		return night;
	}
	public PeriodOfTime getDayShort(){
		return dayShort;
	}
	public PeriodOfTime getEvening(){
		return evening;
	}
	public PeriodOfTime getDay(){
		return day;
	}
	public PeriodOfTime getNightShort(){
		return nightShort;
	}
	public PeriodOfTime getMorning(){
		return morning;
	}
}