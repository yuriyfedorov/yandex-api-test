package shema;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
		"name",
		"abbr",
		"offset",
		"dst"
})
public class Tzinfo {

	@JsonProperty("name")
	public String name;
	@JsonProperty("abbr")
	public String abbr;
	@JsonProperty("offset")
	public int offset;
	@JsonProperty("dst")
	public boolean dst;

	public int getOffset(){
		return offset;
	}
	public boolean isDst(){
		return dst;
	}
	public String getName(){
		return name;
	}
	public String getAbbr(){
		return abbr;
	}
}