package shema;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
		"f",
		"n",
		"nr",
		"ns",
		"nsr",
		"p",
		"lat",
		"lon",
		"tzinfo",
		"def_pressure_mm",
		"def_pressure_pa",
		"_h",
		"url"
})
public class Info {

	@JsonProperty("f")
	public boolean f;
	@JsonProperty("n")
	public boolean n;
	@JsonProperty("nr")
	public boolean nr;
	@JsonProperty("ns")
	public boolean ns;
	@JsonProperty("nsr")
	public boolean nsr;
	@JsonProperty("p")
	public boolean p;
	@JsonProperty("lat")
	public float lat;
	@JsonProperty("lon")
	public float lon;
	@JsonProperty("tzinfo")
	public Tzinfo tzinfo;
	@JsonProperty("def_pressure_mm")
	public int defPressureMm;
	@JsonProperty("def_pressure_pa")
	public int defPressurePa;
	@JsonProperty("_h")
	public boolean h;
	@JsonProperty("url")
	public String url;

	public boolean isNr(){
		return nr;
	}
	public boolean isNs(){
		return ns;
	}
	public boolean isF(){
		return f;
	}
	public int getDefPressureMm(){
		return defPressureMm;
	}
	public boolean isH(){
		return h;
	}
	public double getLon(){
		return lon;
	}
	public boolean isNsr(){ return nsr;	}
	public boolean isN(){ return n;	}
	public Tzinfo getTzinfo(){ return tzinfo; }
	public String getUrl(){ return url;	}
	public boolean isP(){ return p;	}
	public int getDefPressurePa(){ return defPressurePa; }
	public double getLat(){	return lat;	}
}